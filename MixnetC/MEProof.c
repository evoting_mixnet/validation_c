//gcc -g -lssl MEProof.c -lcrypto -o MEp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int BNPrint(const unsigned char *index, BIGNUM *x, int debug){
	    if (debug > 0){  
		printf("%s:",index);
	        BN_print_fp(stdout, x);
		printf("\n");
	    }
}

int Hex2Bn(FILE *fp, BIGNUM *G){	
	char * buffer = malloc((BN_num_bytes(G)+1) * sizeof(char));
	memset(buffer,'\0',(BN_num_bytes(G)+1) * sizeof(char));
	int len = BN_bn2bin(G, buffer);
	fprintf(fp,"%s", buffer);
}
 
int MEProof(const unsigned char *int_fp, const unsigned char *out_fp1, int debug){

    FILE *fp, *fp1_w, *fp2_w;

    char *line = NULL;

    size_t len = 0;
    ssize_t read;
    int k, err =0;

    BN_CTX *ctx;
    BIGNUM *cA, *q, *p, *x, *cA0, *h, *r, *g, *g1, *Cg, *C, *a;
    BIGNUM *cB0, *cB1, *b, *s, *Eg0, *Ey0, *Eg1, *Ey1;
    BIGNUM *tau, *pkg, *pky, *gtau,  *res1, *res2;  
    
    ctx = BN_CTX_new();
      
    cA = BN_new();
    q = BN_new();
    p = BN_new();
    x = BN_new();
    cA0 = BN_new();
    h = BN_new();
    r = BN_new();
    g = BN_new();
    g1 = BN_new();
    Cg = BN_new();
    C = BN_new();
    a = BN_new();
    cB0 = BN_new();
    cB1 = BN_new();
    b = BN_new();
    s = BN_new();
    Eg0 = BN_new();
    Ey0 = BN_new();
    Eg1 = BN_new();
    Ey1 = BN_new();
    tau = BN_new();
    pkg = BN_new();
    pky = BN_new();
    gtau = BN_new();
    res1 = BN_new();
    res2 = BN_new();

    BN_one(res1);
    BN_one(res2);

    //EC_POINT_get_hashed(group,P,ctx,"abcdefgh");   
 
    fp = fopen(int_fp,"r");
    fp1_w = fopen(out_fp1, "w");

    if (fp == NULL) goto err;

    int i=0, linenum = 0; 

    while ((read = getline(&line, &len, fp)) != -1) {
	if (linenum == 0){
	    k = atoi(line);
	    if (debug > 0) printf("k:%d",k); 
	}

	if (linenum == 1){
	    BN_dec2bn(&cA, line);
	    BNPrint("cA:",cA,debug);
	}

	if (linenum == 2){
	    BN_dec2bn(&q, line);
	    BN_add(p,q,q);
	    BN_add(p,p,res1); // using res1 to 1
	    if (debug > 0){
	        printf("q:"); 
	        BN_print_fp(stdout, q);
		printf("\np:"); 
	        BN_print_fp(stdout, p);
	    }	
	}

	if (linenum == 3){
	    BN_dec2bn(&x, line);
	    if (debug > 0){
	    	printf("x:");
	    	BN_print_fp(stdout, x);
	    }
	}

        if (linenum == 4){
	    BN_dec2bn(&cA0, line);
	    BNPrint("cA0:",cA0,debug);
	    BN_mod_exp(cA, cA, x, p, ctx);
	    BN_mod_mul(cA0, cA0, cA, p, ctx);  //cA0 = cA0 * cA^x
	    BN_print_fp(fp1_w,cA0);  //cA0 = cA0 * cA^x
	    fprintf(fp1_w,"\n");
	}


        if (linenum == 5){
	    BN_dec2bn(&h, line);
	    BNPrint("h:",h, debug);
	}

	if (linenum == 6){
	    BN_dec2bn(&r, line);
	    BNPrint("r:",r, debug);
	    BN_mod_exp(r,h,r,p,ctx); //r = h^r
	}

	if (linenum > 6 && (linenum - 7) % 4 == 0 && linenum < (4*k + 7)){
	    BN_dec2bn(&g, line);
	    BNPrint("g:",g, debug);
	    if (linenum == 7) {
		BN_copy(g1,g);
	    }
	}

	if (linenum > 6 && (linenum -7) % 4 == 1 && linenum < (4*k + 7)){
	    BN_dec2bn(&Cg, line);
	    BNPrint("Cg:",Cg, debug);
	}

	if (linenum > 6 && (linenum -7) % 4 == 2 && linenum < (4*k + 7)){
	    BN_dec2bn(&C, line);
	    BNPrint("C:",C, debug);
	    
	}

	if (linenum > 6 && (linenum -7) % 4 == 3 && linenum < (4*k + 7)){
	    BN_dec2bn(&a, line);
	    BNPrint("a:",a, debug);
	    BN_mod_exp(g,g,a,p,ctx); //g = g^a
	    BN_mod_exp(Cg,Cg,a,p,ctx); //Cg = Cg^a where CT = (Cg, C)
	    BN_mod_exp(C,C,a,p,ctx); //C = C^a C^{x^{m-1}a}, but m is fixed to 1. ==> C^a
	    BN_mod_mul(r,r,g,p,ctx); //commitment accumulated
	    BN_mod_mul(res1,res1,Cg,p,ctx); //ciphertext g accumulated
	    BN_mod_mul(res2,res2,C,p,ctx); //ciphertext accumulated 
	}
	
	if (linenum == 4*k+7){
	    BN_dec2bn(&b, line);
	    BNPrint("b:",b, debug);
	    BN_mod_exp(g1,g1,b,p,ctx); //g1 = g1^b
	    
            BN_print_fp(fp1_w,r);  //com_ck(a,r)
	    fprintf(fp1_w,"\n");		
	
	}

	if (linenum == 4*k+8){
	    BN_dec2bn(&s, line);
	    BNPrint("s:",s, debug);
	    BN_mod_exp(s,h,s,p,ctx); //s = h^s
	    BN_mod_mul(g1,g1,s,p,ctx); //com_ck(b,s)	

	    
	    BN_print_fp(fp1_w,g1);  //com_ck(b,s)
	    fprintf(fp1_w,"\n");		

	}

	if (linenum == 4*k+9){
	    BN_dec2bn(&cB0, line);
	    BNPrint("cB0:",cB0, debug);
	}

	if (linenum == 4*k+10){
	    BN_dec2bn(&cB1, line);
	    BNPrint("cB1:",cB1, debug);
	    BN_mod_exp(cB1,cB1,x,p,ctx); //cB1 = cB1^x
	    BN_mod_mul(cB0,cB0,cB1,p,ctx); //cB0 = cB0 * cB1^x
	    BN_print_fp(fp1_w,cB0); //cB0 * C_Bk^xk
	    fprintf(fp1_w,"\n");		
	}

        if (linenum == 4*k+11){
	    BN_dec2bn(&Eg0, line);
	    BNPrint("Eg0:",Eg0, debug);
	}
	if (linenum == 4*k+12){
	    BN_dec2bn(&Ey0, line);
	    BNPrint("Ey0:",Ey0, debug);
	}
	
	if (linenum == 4*k+13){
	    BN_dec2bn(&Eg1, line);
	    BNPrint("Eg1:",Eg1, debug);
	    BN_mod_exp(Eg1,Eg1,x,p,ctx);
	    BN_mod_mul(Eg0,Eg0,Eg1,p,ctx); //g part of E0 * E_1^x
	    BN_print_fp(fp1_w,Eg0); //Y part of E0 * E_1^x
	    fprintf(fp1_w,"\n");		
	
	}
	if (linenum == 4*k+14){
	    BN_dec2bn(&Ey1, line);
	    BNPrint("Ey1:",Ey1, debug);
	    BN_mod_exp(Ey1,Ey1,x,p,ctx);
	    BN_mod_mul(Ey0,Ey0,Ey1,p,ctx);
	    BN_print_fp(fp1_w,Ey0); //Y part of E0 * E_1^x
	    fprintf(fp1_w,"\n");		

	}

	if (linenum == 4*k+15){
	    BN_dec2bn(&tau, line);
	    BNPrint("tau:",tau, debug);
	}

	if (linenum == 4*k+16){
	    BN_dec2bn(&pkg, line);
	    BNPrint("pkg:",pkg, debug);
	    BN_mod_exp(gtau,pkg,tau,p,ctx);
	    BN_mod_mul(res1,res1,gtau,p,ctx);
	    BN_print_fp(fp1_w,res1);  //g part of C_i^a_i 
	    fprintf(fp1_w,"\n");		
	}

	if (linenum == 4*k+17){
	    BN_hex2bn(&pky, line);
	    BNPrint("pky:",pky, debug);
	    BN_mod_exp(pky,pky,tau,p,ctx);
	    BN_mod_exp(pkg,pkg,b,p,ctx);
	    BN_mod_mul(res2,res2,pkg,p,ctx);
	    BN_mod_mul(res2,res2,pky,p,ctx);	
	    BN_print_fp(fp1_w,res2);  //Y part of C_i^a_i
	    fprintf(fp1_w,"\n");		

	}
	
	linenum++;		
  	if (debug > 0) printf("\n");
    //    BN_dec2bn(&X, line);
    }
    
err:
    fclose(fp);
    fclose(fp1_w);

    if (line) free(line);

    BN_free(g1);
    BN_free(cA);
    BN_free(q);
    BN_free(p);
    BN_free(x);
    BN_free(cA0);
    BN_free(h);
    BN_free(r);
    BN_free(g);
    BN_free(Cg);
    BN_free(C);
    BN_free(a);
    BN_free(cB0);
    BN_free(cB1);
    BN_free(b);
    BN_free(s);
    BN_free(Eg0);
    BN_free(Ey0);
    BN_free(Eg1);
    BN_free(Ey1);
    BN_free(tau);
    BN_free(pkg);
    BN_free(pky);
    BN_free(gtau);
    BN_free(res1);
    BN_free(res2);

    return err;
}

int main(int argc, char** argv)
{
    char *filename, *outfilename;

    /* Networking stuff */
    if (argc > 3) {
        filename = argv[1];
	outfilename = argv[2];
	int debug = atoi(argv[2]);
        MEProof(filename, outfilename, debug);
    }else {
	printf("./test [filename] [outfilename] [debug = 0 or 1]");		
    }	

}

