//gcc -g -lssl SVAproof.c -lcrypto -o SVAp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>
int BNPrint(const unsigned char *index, BIGNUM *x, int debug){
	    if (debug > 0){  
		printf("%s:",index);
	        BN_print_fp(stdout, x);
		printf("\n");
	    }
}

int Hex2Bn(FILE *fp, BIGNUM *G){	
	char * buffer = malloc((BN_num_bytes(G)+1) * sizeof(char));
	memset(buffer,'\0',(BN_num_bytes(G)+1) * sizeof(char));
	int len = BN_bn2bin(G, buffer);
	fprintf(fp,"%s", buffer);
}
 
int SVAProof(const unsigned char *int_fp, const unsigned char *out_fp1, int debug){

    FILE *fp, *fp1_w, *fp2_w;

    char *line = NULL;

    size_t len = 0;
    ssize_t read;
    int k, err =0;

    BN_CTX *ctx;
    BIGNUM *g1, *pg1, *ts, *tr, *a, *b, *pb, *res, *res2, *p;
    BIGNUM *x, *cb, *cd, *cldel, *cHDel, *h, *q, *BNOne; 
    
    ctx = BN_CTX_new();
      
    p = BN_new();
    q = BN_new();
    g1 = BN_new();
    pg1 = BN_new();
    ts = BN_new();
    tr = BN_new();

    x = BN_new();
    cb = BN_new();
    cd = BN_new();
    cldel = BN_new();
    cHDel = BN_new();
    h = BN_new();

    a = BN_new();
    b = BN_new();
    pb = BN_new();

    res = BN_new();
    res2 = BN_new();
    BNOne = BN_new();

    BN_one(BNOne);

    //EC_POINT_get_hashed(group,P,ctx,"abcdefgh");   
 
    fp = fopen(int_fp,"r");
    fp1_w = fopen(out_fp1, "w");

    if (fp == NULL) goto err;

    int i=0, linenum = 0; 

    while ((read = getline(&line, &len, fp)) != -1) {
	if (linenum == 0){
	    k = atoi(line);
	    if (debug > 0) fprintf(stdout, "k:%d",k); 
	}
 
	if (linenum == 1){
	    BN_dec2bn(&q, line);
	    BN_add(p,q,q);
	    BN_add(p,p,BNOne);
	    if (debug > 0){
	        printf("q:"); 
	        BN_print_fp(stdout, q);
		printf("\np:"); 
	        BN_print_fp(stdout, p);
	    }	
	}

	if (linenum == 2){
	    BN_hex2bn(&x, line);
	    if (debug > 0){
	    	printf("x:");
	    	BN_print_fp(stdout, x);
	    }
	}

        if (linenum == 3){
	    BN_dec2bn(&cb, line);
	    if (debug > 0){
	        printf("cb:");
	        BN_print_fp(stdout, cb);
     	    }
	    BN_mod_exp(cb, cb, x, p, ctx);
	}


        if (linenum == 4){
	    BN_dec2bn(&cd, line);
	    BNPrint("cd:",cd, debug);
	    BN_mod_mul(cb, cb, cd, p, ctx);
	    BN_print_fp(fp1_w,cb);
	    fprintf(fp1_w,"\n");
	}


        if (linenum == 5){
	    BN_dec2bn(&cldel, line);
	    if (debug >0 ){
		printf("cldel:");
		BN_print_fp(stdout, cldel);
	    }
	}


        if (linenum == 6){
	    BN_dec2bn(&cHDel, line);
	    if (debug >0 ){
		printf("cHDel:");
	    	BN_print_fp(stdout, cHDel);	    
	    }
	    BN_mod_exp(cHDel, cHDel, x, p, ctx);
	    BN_mod_mul(cldel, cldel, cHDel, p, ctx);
	    BN_print_fp(fp1_w,cldel);
	    fprintf(fp1_w,"\n");
	}

	if (linenum == 7){
	    BN_dec2bn(&h, line);
	    if (debug >0 ){
	        printf("h:");
	        BN_print_fp(stdout, h);
	    }
	}

	if (linenum == 8){
	    BN_dec2bn(&tr, line);
	    BNPrint("tr:",tr, debug);
	    BN_mod_exp(tr, h,tr,p,ctx);
	}

	if (linenum == 9){
	    BN_dec2bn(&ts, line);
	    BNPrint("ts:",ts, debug);
	    BN_mod_exp(ts, h,ts,p,ctx);
	}

	if ((linenum > 9) && (linenum - 10) % 3 == 0){
	    if (i > 0) BN_copy(pg1, g1);
	    BN_dec2bn(&g1,line);
	    BNPrint("g:",g1, debug);
	}
	
        if ((linenum > 9) && (linenum - 10) % 3 == 1){
	    BN_dec2bn(&a,line);
	    BNPrint("a:",a, debug);
	    BN_mod_exp(res,g1,a,p,ctx); // g_i^a_i
	    BN_mod_mul(tr,res,tr,p,ctx); // h^tr * g_i^a_i ...
	}

  	if ((linenum > 9) && (linenum - 10) % 3 == 2){
	    if (i > 0) BN_copy(pb,b);		//b_i-1 =bi
	    BN_dec2bn(&b,line);
	    BNPrint("b:",b, debug);
	    if(i >0){
	    	BN_mod_mul(res, x,b,q,ctx);	//xb_2
	    	BN_mod_mul(res2, pb,a,q,ctx);   //a_2 b_1
	    	BN_mod_sub(res, res, res2,q,ctx); // xb_2 - a_2b_1
	    	BN_mod_exp(res2,pg1,res,p,ctx);  //g1^{xb_2 - a_2b_1}
	    	BN_mod_mul(ts,ts,res2,p,ctx);  //ts = h^ts * g1^{xb_2 - a_2b_1}
	    }
	    if (i == (k -1)) {
		BN_print_fp(fp1_w,tr);
	    	fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w,ts);
	    	fprintf(fp1_w,"\n");
	    }
	    i++;
	}

	linenum++;		
  	if (debug >0 ) printf("\n");
    }
    
err:
    fclose(fp);
    fclose(fp1_w);

    if (line) free(line);
    BN_free(g1);
    BN_free(p);
    BN_free(q);
    BN_free(pg1);
    BN_free(ts);
    BN_free(tr);

    BN_free(x);
    BN_free(cb);
    BN_free(cd);
    BN_free(cldel);
    BN_free(cHDel);
    BN_free(h);

    BN_free(a);
    BN_free(b);
    BN_free(pb);

    BN_free(res);
    BN_free(res2);
    BN_free(BNOne);
    return err;
}

int main(int argc, char** argv)
{
    char *filename;

    /* Networking stuff */
    if (argc > 2) {
        filename = argv[1];
	int debug = atoi(argv[2]);
        SVAProof(filename, "resSV.txt", debug);
    }else {
	printf("./test [filename] [debug = 0 or 1]");		
    }	

}

