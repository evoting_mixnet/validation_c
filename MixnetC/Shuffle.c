//gcc -g -lssl Shuffle.c -lcrypto -o SHFp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>
int BNPrint(const unsigned char *index, BIGNUM *x, int debug){
	    if (debug > 0){  
		printf("%s:",index);
	        BN_print_fp(stdout, x);
		printf("\n");
	    }
}

int Hex2Bn(FILE *fp, BIGNUM *G){	
	char * buffer = malloc((BN_num_bytes(G)+1) * sizeof(char));
	memset(buffer,'\0',(BN_num_bytes(G)+1) * sizeof(char));
	int len = BN_bn2bin(G, buffer);
	fprintf(fp,"%s", buffer);
}
 
int SHProof(const unsigned char *int_fp, const unsigned char *out_fp1, int debug){

    FILE *fp, *fp1_w, *fp2_w;

    char *line = NULL;

    size_t len = 0;
    ssize_t read;
    int k, err =0;

    BN_CTX *ctx;
    BIGNUM  *p, *q, *BNOne;
    BIGNUM *x, *y, *z, *x1, *y1, *b, *h, *g;
    
    BIGNUM *cA, *cB, *cD, *gc, *gam, *phis;

    BIGNUM *resgam, *resphis,*resphis2, *res, *nz;
    
    ctx = BN_CTX_new();
      
    p = BN_new();
    q = BN_new();
    BNOne = BN_new();

    x = BN_new();
    y = BN_new();
    z = BN_new();
    nz = BN_new();
    x1 = BN_new();
    y1 = BN_new();
    b = BN_new();    
    h = BN_new(); 
    g = BN_new(); 

    cA = BN_new();
    cB = BN_new();

    gc = BN_new();
    gam = BN_new();
    phis = BN_new();
   
    resgam = BN_new();
    resphis = BN_new();
    resphis2 = BN_new();
    res = BN_new();

    BN_one(BNOne);
    BN_one(resgam);
    BN_one(resphis);
    BN_one(resphis2);
    BN_one(res);
    BN_one(y1);


    //EC_POINT_get_hashed(group,P,ctx,"abcdefgh");   
 
    fp = fopen(int_fp,"r");
    fp1_w = fopen(out_fp1, "w");

    if (fp == NULL) goto err;

    int i=0, linenum = 0; 

    while ((read = getline(&line, &len, fp)) != -1) {
	if (linenum == 0){
	    k = atoi(line);
	    if (debug > 0) printf("k:%d",k); 
	}
	
	if (linenum == 1){
	    BN_dec2bn(&q, line);
	    BN_add(p,q,q);
	    BN_add(p,p,BNOne);
	    if (debug > 0){
	        printf("q:"); 
	        BN_print_fp(stdout, q);
		printf("\np:"); 
	        BN_print_fp(stdout, p);
	    }	
	}

	if (linenum == 2){
	    BN_hex2bn(&x, line);
	    if (debug > 0){
	    	printf("x:");
	    	BN_print_fp(stdout, x);
	    }
	    BN_copy(res,x);
	}

	if (linenum == 3){
	    BN_hex2bn(&y, line);
	    if (debug > 0){
	    	printf("y:");
	    	BN_print_fp(stdout, y);
	    }
	}

	if (linenum == 4){
	    BN_hex2bn(&z, line);
	    BN_sub(nz,q,z); // z = -z
	    if (debug > 0){
	    	printf("z:");
	    	BN_print_fp(stdout, z);
	    }
	}


	if (linenum == 5){
	    BN_dec2bn(&b, line);
	    if (debug > 0){
	    	printf("b:");
	    	BN_print_fp(stdout, b);
	    }
	}

	if (linenum == 6){
	    BN_dec2bn(&cA, line);
	    if (debug > 0){
	    	printf("cA:");
	    	BN_print_fp(stdout, cA);
	    }
	}

	if (linenum == 7){
	    BN_dec2bn(&cB, line);
	    if (debug > 0){
	    	printf("cB:");
	    	BN_print_fp(stdout, cB);
	    }
	    BN_mod_exp(cA,cA,y,p,ctx); // cA^y
	    BN_mod_mul(cA,cA,cB,p,ctx); //cD = cA = cA^y *cB
	}

	if (linenum == 8){
	    BN_dec2bn(&h, line);
	    if (debug > 0){
	    	printf("h:");
	    	BN_print_fp(stdout, h);
	    }
	}

	if ((linenum > 8) && (linenum - 9) % 4 == 0){
	    BN_dec2bn(&gc,line);
	    BNPrint("g:",gc, debug);
	    BN_mod_mul(BNOne,BNOne,gc,p,ctx);     
	}
	
	if ((linenum > 8) && (linenum - 9) % 4 == 1){
	    BN_dec2bn(&gam,line);
	    BNPrint("gamma:",gam, debug);
	    BN_mod_exp(gam,gam,res,p,ctx);
	    BN_mod_mul(resgam,resgam,gam,p,ctx); 
	}

        if ((linenum > 8) && (linenum - 9) % 4 == 2){
	    BN_dec2bn(&phis,line);
	    BNPrint("phis:",phis, debug);
	    BN_mod_exp(phis,phis,res,p,ctx); 
	    BN_mod_mul(resphis,resphis,phis,p,ctx);
	}	

        if ((linenum > 8) && (linenum - 9) % 4 == 3){
	    BN_dec2bn(&phis,line);
	    BNPrint("phis:",phis, debug);
	    BN_mod_exp(phis,phis,res,p,ctx); 
	    BN_mod_mul(resphis2,resphis2,phis,p,ctx);
	
	    i++;	
	    BN_set_word(x1,i);
	    BN_mod_mul(x1,y,x1,q,ctx);
	    BN_mod_add(x1,x1,res,q,ctx);
	    BN_mod_sub(x1,x1,z,q,ctx);
	    BN_mod_mul(y1,y1,x1,q,ctx); 
	    BN_mod_mul(res,res,x,q,ctx); 

	    if (i == k){
		BN_print_fp(fp1_w,BNOne);
	    	fprintf(fp1_w,"\n");
    		BN_mod_exp(BNOne,BNOne,nz,p,ctx);
		BN_mod_mul(cA,cA,BNOne,p,ctx);
		BN_print_fp(fp1_w,cA);
	    	fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w,resgam);
		fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w,resphis);
		fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w,resphis2);
		fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w,y1);
		fprintf(fp1_w,"\n");
	    }
	}

	linenum++;		
  	if (debug > 0) printf("\n");
    //    BN_dec2bn(&X, line);
    }
err:
    fclose(fp);
    fclose(fp1_w);

    p = BN_new();
    q = BN_new();
    BNOne = BN_new();

    x = BN_new();
    y = BN_new();
    z = BN_new();
    x1 = BN_new();
    y1 = BN_new();
    b = BN_new();    
    h = BN_new(); 
    g = BN_new(); 

    cA = BN_new();
    cB = BN_new();

    gc = BN_new();
    gam = BN_new();
    phis = BN_new();
   
    resgam = BN_new();
    resphis = BN_new();
    res = BN_new();

    if (line) free(line);
    BN_free(p);
    BN_free(BNOne);
    BN_free(q);
    BN_free(x);
    BN_free(y);
    BN_free(z);
    BN_free(nz);

    BN_free(x1);
    BN_free(y1);

    BN_free(b);
    BN_free(h);
    BN_free(g);
    BN_free(cA);
    BN_free(cB);

    BN_free(gc);
    BN_free(gam);
    BN_free(phis);

    BN_free(resgam);
    BN_free(resphis);
    BN_free(res);
    return err;
}

int main(int argc, char** argv)
{
    char *filename;

    /* Networking stuff */
    if (argc > 2) {
        filename = argv[1];
	int debug = atoi(argv[2]);
        SHProof(filename, "resSHF.txt", debug);
    }else {
	printf("./SHFp [filename] [debug = 0 or 1]");		
    }	

}

