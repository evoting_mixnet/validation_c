//gcc -g -lssl ShuffleHP.c -lcrypto -o HPp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>
int BNPrint(const unsigned char *index, BIGNUM *x, int debug){
	    if (debug > 0){  
		printf("%s:",index);
	        BN_print_fp(stdout, x);
		printf("\n");
	    }
}

int Hex2Bn(FILE *fp, BIGNUM *G){	
	char * buffer = malloc((BN_num_bytes(G)+1) * sizeof(char));
	memset(buffer,'\0',(BN_num_bytes(G)+1) * sizeof(char));
	int len = BN_bn2bin(G, buffer);
	fprintf(fp,"%s", buffer);
}
 
int SHProof(const unsigned char *int_fp, const unsigned char *out_fp1, int debug){

    FILE *fp, *fp1_w, *fp2_w;

    char *line = NULL;

    size_t len = 0;
    ssize_t read;
    int k, err =0;

    BN_CTX *ctx;
    BIGNUM *p, *q, *BNOne;
    BIGNUM *x, *y;
    
    BIGNUM *cA2, *gc, *cB1, *cB2;

    BIGNUM *res;
    
    ctx = BN_CTX_new();
      
    p = BN_new();
    q = BN_new();
    BNOne = BN_new();

    x = BN_new();
    y = BN_new();
    
    cA2 = BN_new();
    gc = BN_new();
    cB1 = BN_new();
    cB2 = BN_new();
     
    res = BN_new();

    BN_one(res);
    BN_one(BNOne);
    //EC_POINT_get_hashed(group,P,ctx,"abcdefgh");   
 
    fp = fopen(int_fp,"r");
    fp1_w = fopen(out_fp1, "w");

    if (fp == NULL) goto err;

    int i=0, linenum = 0; 

    while ((read = getline(&line, &len, fp)) != -1) {
	if (linenum == 0){
	    k = atoi(line);
	    if (debug > 0) printf("k:%d",k); 
	}
	
	if (linenum == 1){
	    BN_dec2bn(&q, line);
	    BN_add(p,q,q);
	    BN_add(p,p,BNOne);
	    if (debug > 0){
	        printf("q:"); 
	        BN_print_fp(stdout, q);
		printf("\np:"); 
	        BN_print_fp(stdout, p);
	    }	
	}

	if (linenum == 2){
	    BN_hex2bn(&x, line);
	    if (debug > 0){
	    	printf("x:");
	    	BN_print_fp(stdout, x);
	    }
	}

	if (linenum == 3){
	    BN_hex2bn(&y, line);
	    if (debug > 0){
	    	printf("y:");
	    	BN_print_fp(stdout, y);
	    }
	}

	if (linenum == 4){
	    BN_hex2bn(&cA2, line);
	    if (debug > 0){
	    	printf("cA2:");
	    	BN_print_fp(stdout, cA2);
	    }
	    BN_print_fp(fp1_w,cA2);
	    fprintf(fp1_w,"\n");
	}

	if (linenum == 5){
	    BN_dec2bn(&cB1, line);
	    if (debug > 0){
	    	printf("cB1:");
	    	BN_print_fp(stdout, cB1);
	    }
	    BN_mod_exp(cB1,cB1,x,p,ctx);
	    BN_print_fp(fp1_w,cB1);
	    fprintf(fp1_w,"\n");
	}

	if (linenum == 6){
	    BN_dec2bn(&cB2, line);
	    if (debug > 0){
	    	printf("cB2:");
	    	BN_print_fp(stdout, cB2);
	    }
	    BN_mod_exp(cB2,cB2,x,p,ctx);
	    BN_print_fp(fp1_w,cB2);
	    fprintf(fp1_w,"\n");
	}

	if ((linenum > 6) && (linenum < 7 + k)){
	    BN_dec2bn(&gc,line);
	    BNPrint("g:",gc, debug);
	    BN_mod_mul(res,res,gc,p,ctx);
	    
	    if (linenum == (6 + k)){
		BN_sub(BNOne, q, BNOne);
		BN_mod_exp(res,res,BNOne,p,ctx);
	    	BNPrint("cA2:",res, debug);
		BN_print_fp(fp1_w,res);
	    }
	}

	linenum++;		
  	if(debug) printf("\n");
    //    BN_dec2bn(&X, line);
    }
err:
    fclose(fp);
    fclose(fp1_w);


    if (line) free(line);
    BN_free(p);
    BN_free(q);
    
    BN_free(x);
    BN_free(y);
    BN_free(cA2);
    BN_free(gc);
    BN_free(cB1);
    BN_free(cB2);
    BN_free(res);
    BN_free(BNOne);
    return err;
}

int main(int argc, char** argv)
{
    char *filename;

    /* Networking stuff */
    if (argc > 2) {
        filename = argv[1];
	int debug = atoi(argv[2]);
        SHProof(filename, "resHP.txt", debug);
    }else {
	printf("./test [filename] [debug = 0 or 1]");		
    }	

}

