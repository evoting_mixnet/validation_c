//gcc -g -lssl Zeroproof.c -lcrypto -o ZAp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>
int BNPrint(const unsigned char *index, BIGNUM *x, int debug){
	    if (debug > 0){  
		printf("%s:",index);
	        BN_print_fp(stdout, x);
		printf("\n");
	    }
}

int Hex2Bn(FILE *fp, BIGNUM *G){	
	char * buffer = malloc((BN_num_bytes(G)+1) * sizeof(char));
	memset(buffer,'\0',(BN_num_bytes(G)+1) * sizeof(char));
	int len = BN_bn2bin(G, buffer);
	fprintf(fp,"%s", buffer);
}
 
int ZeroProof(const unsigned char *int_fp, const unsigned char *out_fp1, int debug){

    FILE *fp, *fp1_w, *fp2_w;

    char *line = NULL;

    size_t len = 0;
    ssize_t read;
    int k, err =0;

    BN_CTX *ctx;
    BIGNUM *g, *tt, *ts, *tr, *a, *b, *rescA, *rescB, *rescD;
    BIGNUM *x, *x1, *y, *cA, *cB, *cD, *h, *q, *p, *BNOne, *ga, *g1, *rescoff; 
    
    ctx = BN_CTX_new();
      
    p = BN_new();
    q = BN_new();
    BNOne = BN_new();
    x = BN_new();
    x1 = BN_new();
    y = BN_new();
    ga = BN_new();
    g1 = BN_new(); 
    rescoff = BN_new();

    cA = BN_new();
    cB = BN_new();
    cD = BN_new();
    

    rescA = BN_new();
    rescB = BN_new();
    rescD = BN_new();

    h = BN_new();

    tt = BN_new();
    ts = BN_new();
    tr = BN_new();

    a = BN_new();
    b = BN_new();
    g = BN_new();


    BN_one(BNOne);
    //EC_POINT_get_hashed(group,P,ctx,"abcdefgh");   
 
    fp = fopen(int_fp,"r");
    fp1_w = fopen(out_fp1, "w");

    if (fp == NULL) goto err;

    int i=0, linenum = 0; 

    while ((read = getline(&line, &len, fp)) != -1) {
	if (linenum == 0){
	    k = atoi(line);
	    if (debug > 0) printf("k:%d",k); 
	}
	
	if (linenum == 1){
	    BN_dec2bn(&q, line);
	    BN_add(p,q,q);
	    BN_add(p,p,BNOne);
	    BNPrint("q:",q,debug);
	    BNPrint("p:",p, debug);
	}

	if (linenum == 2){
	    BN_hex2bn(&x, line);
	    if (debug > 0){
	    	printf("x:");
	    	BN_print_fp(stdout, x);
	    	fprintf(stdout,"\n");
	    }
	}

	if (linenum == 3){
	    BN_hex2bn(&y, line);
	    if (debug > 0){
	    	printf("y:");
	    	BN_print_fp(stdout, y);
	    	fprintf(stdout,"\n");
	    }
	}

        if (linenum == 4){
	    BN_dec2bn(&rescA, line);
	    if (debug > 0){
	        printf("cA:");
	        BN_print_fp(stdout, rescA);
	    	fprintf(stdout,"\n");
     	    }
	}

        if (linenum == 5){
	    BN_dec2bn(&rescB, line);
	    if (debug > 0){
	        printf("cB:");
	        BN_print_fp(stdout, rescB);
	    	fprintf(stdout,"\n");
     	    }
	}

        if (linenum == 6){
	    BN_dec2bn(&rescD, line);
	    if (debug > 0){
	        printf("cD:");
	        BN_print_fp(stdout, rescD);
	    	fprintf(stdout,"\n");
	    }
	}

	if (linenum == 7){
	    BN_hex2bn(&cA, line);
	    BNPrint("cA:",cA, debug);
	    BN_mod_exp(cA, cA, x, p, ctx);
	    BN_mod_mul(rescA, rescA, cA, p, ctx);
	}


        if (linenum == 8){
	    BN_hex2bn(&cB, line);
	    BNPrint("cB:",cB, debug);
	    BN_mod_exp(cB, cB, x, p, ctx);
	    BN_mod_mul(rescB, rescB, cB, p, ctx);
	}

       if (linenum == 9){
	    BN_dec2bn(&cD, line);
	    BNPrint("cD:",cD, debug);
	    BN_mod_exp(cD, cD, x, p, ctx);
	    BN_mod_mul(rescD, rescD, cD, p, ctx);
	    BN_mod_mul(x1,x,x,q,ctx);
	    BNPrint("x:",x1, debug);	    	
	}

	if (linenum == 10){
	    BN_hex2bn(&cA, line);
	    BNPrint("cA:",cA, debug);
	    BN_mod_exp(cA, cA, x1, p, ctx);
	    BN_mod_mul(rescA, rescA, cA, p, ctx);
	    BN_print_fp(fp1_w, rescA);
	    fprintf(fp1_w,"\n");
	}

        if (linenum == 11){
	    BN_hex2bn(&cB, line);
	    BNPrint("cB:",cB, debug);
	    BN_mod_exp(cB, cB, x1, p, ctx);
	    BN_mod_mul(rescB, rescB, cB, p, ctx);
	    BN_print_fp(fp1_w, rescB);
	    fprintf(fp1_w,"\n");

	}

       if (linenum == 12){
	    BN_dec2bn(&cD, line);
	    BNPrint("cD:",cD, debug);
	    BN_mod_exp(cD, cD, x1, p, ctx);
	    BN_mod_mul(rescD, rescD, cD, p, ctx);
	    BN_mod_mul(x1,x1,x,q,ctx);
	    BNPrint("x:",x1, debug);

	}

       if (linenum == 13){
	    BN_dec2bn(&cD, line);
	    BNPrint("cD:",cD, debug);
	    BN_mod_exp(cD, cD, x1, p, ctx);
	    BN_mod_mul(rescD, rescD, cD, p, ctx);
	    BN_mod_mul(x1,x1,x,q,ctx);
	    BNPrint("x:",x1, debug);

	}

       if (linenum == 14){
	    BN_dec2bn(&cD, line);
	    BNPrint("cD:",cD, debug);
	    BN_mod_exp(cD, cD, x1, p, ctx);
	    BN_mod_mul(rescD, rescD, cD, p, ctx);
	    BN_print_fp(fp1_w, rescD);
	    fprintf(fp1_w,"\n");

	}

	if (linenum == 15){
	    BN_dec2bn(&h, line);
	    if (debug > 0 ){
	        printf("h:");
	        BN_print_fp(stdout, h);
	        fprintf(stdout,"\n");
	    }
	}

	if (linenum == 16){
	    BN_dec2bn(&tr, line);
	    BNPrint("tr:",tr, debug);
	    BN_mod_exp(tr, h,tr,p,ctx);
	}

	if (linenum == 17){
	    BN_dec2bn(&ts, line);
	    BNPrint("ts:",ts, debug);
	    BN_mod_exp(ts, h,ts,p,ctx);
	}


	if (linenum == 18){
	    BN_dec2bn(&tt, line);
	    BNPrint("tt:",tt, debug);
	    BN_mod_exp(tt, h,tt,p,ctx);
	}

	if ((linenum > 18) && (linenum - 19) % 3 == 0){
	    BN_dec2bn(&g,line);
	    BNPrint("g:",g, debug);
	    if (i == 0) BN_copy(g1,g);	
	}

	if ((linenum > 18) && (linenum - 19) % 3 == 1){
	    BN_dec2bn(&a,line);
	    BNPrint("a:",a, debug);
	    BN_mod_exp(ga,g,a,p,ctx);
	    BN_mod_mul(tr,tr,ga,p,ctx); //cA
	    //BN_mod_exp(a,g1,a,p,ctx);
	}

	if ((linenum > 18) && (linenum - 19) % 3 == 2){
	    BN_dec2bn(&b,line);
	    BNPrint("b:",b, debug);
	    BN_mod_exp(g,g,b,p,ctx);
	    BN_mod_mul(ts,ts,g,p,ctx); //cB

	    BN_mod_mul(a,a,b,q,ctx);
	    i++;
	    BN_set_word(BNOne,i);
	    BNPrint("yi:",BNOne, debug);
	    BN_mod_exp(BNOne,y,BNOne,q,ctx);
	    BN_mod_mul(a,a,BNOne,q,ctx);
	    BN_mod_exp(a,g1,a,p,ctx);

	    //for (int j = 0; j < i; j++) {
		//BN_mod_exp(a,a,y,p,ctx);
	        //BNPrint("by:",a,  1);//debug);
	    //}	
    
	    BN_mod_mul(tt,tt,a,p,ctx);  //cD
	    
	    if (i == k ){
		BN_print_fp(fp1_w, tr);
	    	fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w, ts);
	    	fprintf(fp1_w,"\n");
		BN_print_fp(fp1_w, tt);
	    	fprintf(fp1_w,"\n");
	    }
	}
	
	linenum++;	
    //    BN_dec2bn(&X, line);
    }
err:
    fclose(fp);
    fclose(fp1_w);

    if (line) free(line);
    BN_free(g);
    BN_free(ga);
    BN_free(g1);
    BN_free(BNOne);

    BN_free(tt);
    BN_free(ts);
    BN_free(tr);
    BN_free(a);
    BN_free(b);
    BN_free(rescA);
    BN_free(rescB);
    BN_free(rescD);
    BN_free(rescoff);

    BN_free(x);
    BN_free(y);

    BN_free(cA);
    BN_free(cB);
    BN_free(cD);

    BN_free(h);
    BN_free(q);
    BN_free(p);

    return err;
}

int main(int argc, char** argv)
{
    char *filename;

    /* Networking stuff */
    if (argc > 2) {
        filename = argv[1];
	int debug = atoi(argv[2]);
        ZeroProof(filename, "resZA.txt", debug);
    }else {
	printf("./ZAp [filename] [debug = 0 or 1]");		
    }	

}

