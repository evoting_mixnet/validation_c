import sys, time
import json
import subprocess
from hashlib import sha256

from pyasn1_modules import pem, rfc2459
from pyasn1.codec.der import decoder
import re

def printf(logging,fw,text):
	fw.write(str(text)+"\n")
	if logging > 0: print(text)

def ReadPK(filename):
	pk1start = False
	pk2start = False
	pk1 =""
	pk2 =""

	substrate = pem.readPemFromFile(open(filename))
	cert = decoder.decode(substrate, asn1Spec=rfc2459.Certificate())[0]
	cert_txt = cert.prettyPrint()
	#print(cert_txt)
	regexp = re.compile('extnValue=0x(\S+)')

	pk = re.findall(regexp, cert_txt)
	#print("pk1:" + str(int(pk[2],16))) 
	#print("pk2:" + str(int(pk[3],16))) 
	
	return [pk[2][8:], pk[3][8:]] 

def genX(MNData, commlen):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[1]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]" 

	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	Xstring = Xstring + "MultiExponentiationBasicProofInitialMessage [_cA0=PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicA0"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], _cB=[PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], PublicCommitment [_commitment=ZpGroupElement [_value=1"
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]], _E=[CiphertextImpl [gamma=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["phis"].split(",")[1]).split(";")[0] 
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]], CiphertextImpl [gamma=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[1]).split(";")[0] 
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]]]"
	
	print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()
	return str(int(res,16))

def MEA(debug,MNData,m,cert,voteorder=0):

	i = 0
	eID = ""

	fw=open("outMEA.txt","w")

	#fct = open("CTData" + str(eID) + "-" +str(i)+".txt","r")
	
	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	
	printf(debug,fw,commlen)
	printf(debug,fw,MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	#printf(debug,fw,MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])	#CA
	printf(debug,fw,MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	
	x = genX(MNData, commlen)
	printf(debug,fw,x) #need x

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicA0"]["element"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentR"]["value"]) #r

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])) #Cg_i		
		printf(debug,fw,(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][voteorder])) #Cy_i
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentsA"][i]["value"]) # a_i

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentsB"]["value"]) #b
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentS"]["value"]) #s

	for i in range(2*m):
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][i]["element"]["value"])	#Bi
	
	#if (i == (2*m)):
		#if (str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][i]["element"]["value"]).replace(" ","") != "1"):
			#print ( "ME is failed!")
	
	for i in range(2*m): #E0 #E1
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][i]["gamma"].split(";")[0])
		printf(debug,fw,(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][i]["phis"].split(",")[voteorder]).split(";")[0]) 		
	
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["randomnessTau"]["randomnessValue"]["value"]) #tau
	
	printf(debug,fw,"2")
	printf(debug,fw,cert[voteorder]) #tau
	#printf(debug,fw,cert[1]) #tau
	fw.close()

if __name__ == '__main__':
	if (len(sys.argv) < 3):
		print len(sys.argv)
		print ("Usage: $python Decrypt_proof.py [Dection_Proof_Filename] [Certificate]")
		exit(0)
	else:
		filename = str(sys.argv[1])
		frDE = open(filename,"r")#"Decrypt/decryptionProof-SB1812.json", "r")
		frcert = ReadPK(str(sys.argv[2]))#ReadPK("Cert/VerifiableMixingElectoralBoard.cer")

		JData = frDE.read()
		MNData = json.loads(JData)
		MEA(0,MNData,1,frcert,0)
		subprocess.call(["./MEProof", "outMEA.txt", "1"])

