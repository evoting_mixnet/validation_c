import sys, time
import json
import subprocess
from multiprocessing import Process
from pyasn1_modules import pem, rfc2459
from pyasn1.codec.der import decoder
from Crypto.Hash import SHA512
import re, glob

def proof(filelist,certname,debug):
	for filename in filelist:
		print ["python", "Shuffle.py", filename, certname, str(debug)]
		subprocess.call(["python", "Shuffle.py", filename, certname, str(debug)])

if __name__ == '__main__':
	if (len(sys.argv) < 4):
		print len(sys.argv)
		print ("Usage: $python mixing_proof.py [Mixing_Proof_Foldername] [Certificate] [Order]")
		exit(0)
	else:
		foldername = str(sys.argv[1])
		#frDE = open(filename,"r")#"Decrypt/decryptionProof-SB1812.json", "r")
		#frcert = ReadPK(str(sys.argv[2]))#ReadPK("Cert/VerifiableMixingElectoralBoard.cer")
	
	filelist = glob.glob(foldername + "/*.json")
	filelist1 = []
	#filelist2 = []
	#filelist3 = []
	#i = 0
	order = int(sys.argv[3])
	print len(filelist)
	for filename in filelist:
		filelist1.append(filename)		
		#if (i % 3 == order): filelist1.append(filename)
		#if (i % 3 == 1): filelist2.append(filename)
		#if (i % 3 == 2): filelist3.append(filename)
		#i = i +1  
	proof(filelist1,sys.argv[2],0)

	#p = Process(target=proof, args=(filelist1,sys.argv[2],0))
	#p1 = Process(target=proof, args=(filelist2,sys.argv[2],0))
	#p2 = Process(target=proof, args=(filelist3,sys.argv[2],0))
		
	#p.start()
	#p1.start()
	#p2.start()
	
	#p.join()
	#p1.join()
	#p2.join()

