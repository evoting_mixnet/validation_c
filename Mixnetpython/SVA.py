import json
import subprocess
from hashlib import sha256

def printf(logging,fw,text):
	fw.write(str(text)+"\n")
	if logging > 0: print(text)

def genXYZ(MNData, commlen):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	

	#Value C	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	#Value C'	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	resX = h.hexdigest()
	
	Ystring = Xstring	
	
	Ystring = Ystring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Ystring = Ystring + str(MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	Ystring = Ystring + ", _p=" + p + ", _q=" + q + "]]"

	#print Ystring
	b = Ystring.encode('utf-8')
	hy = sha256()
	hy.update(b)
	resY = hy.hexdigest()

	Zstring = Ystring + "1"

	#print Zstring
	b = Zstring.encode('utf-8')
	hz = sha256()
	hz.update(b)
	resZ = hz.hexdigest()
	
	return [resX, resY, resZ]

def genX(MNData, b):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])

	Xstring = ""	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	Xstring = Xstring + "Exponent [_q=" + q +", _value="
	Xstring = Xstring + b +"]SingleValueProductProofInitialMessage [cd=PublicCommitment [_commitment=ZpGroupElement [_value="
 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicD"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], commitmentPublicLowDelta=PublicCommitment [_commitment=ZpGroupElement [_value="

	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicLowDelta"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], commitmentPublicHighDelta=PublicCommitment [_commitment=ZpGroupElement [_value="
	
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicHighDelta"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"

	print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	resX = h.hexdigest()
		
	return resX

def SVA(debug,MNData):
	fw=open("outSVA.txt","w")
	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = MNData["content"]["proofs"]["commitmentParams"]["h"]["q"]
	printf(debug,fw,int(q))

	XYZ = genXYZ(MNData, commlen)

	b = 1 
	X = int(XYZ[0],16)
	for i in range(commlen):
		temp = (int(XYZ[1],16) * (i + 1) + X - int(XYZ[2],16))
		X = X * X
		b = b * temp	
	
	b = b % int(q)

	X = genX(MNData, str(b))
	print(b)
	#printf(debug,fw,str(b))
	printf(debug,fw,str(X))

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicD"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicLowDelta"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicHighDelta"]["element"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentTildeR"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentTildeS"]["value"])

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentsTildeA"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentsTildeB"][i]["value"])

	fw.close()
	subprocess.call(["./SVAp", "outSVA.txt", str(1)])	

def main():
	fr=open("/media/sf_iVote/mixing-ouput-8a84843b67973e3801679741f88b00a2-WEB-0.json", "r")
	JData = fr.read()
	MNData = json.loads(JData)	
	SVA(0,MNData)
	#ZeroA(1,MNData,m)	
	#MEA(0,MNData,1,DecP)

main()
