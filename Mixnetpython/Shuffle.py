import json, sys
import time
import subprocess
from hashlib import sha256

from pyasn1_modules import pem, rfc2459
from pyasn1.codec.der import decoder
import re

def printf(logging,fw,text):
	fw.write(str(text)+"\n")
	if logging > 0: print(text)

def ReadPK(filename):
	pk1start = False
	pk2start = False
	pk1 =""
	pk2 =""

	substrate = pem.readPemFromFile(open(filename))
	cert = decoder.decode(substrate, asn1Spec=rfc2459.Certificate())[0]
	cert_txt = cert.prettyPrint()
	#print(cert_txt)
	regexp = re.compile('extnValue=0x(\S+)')

	pk = re.findall(regexp, cert_txt)
	#print("pk1:" + str(int(pk[2],16))) 
	#print("pk2:" + str(int(pk[3],16))) 
	
	return [pk[2][8:], pk[3][8:]] 

def MEgenX(MNData, commlen):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[1]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]" 

	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	Xstring = Xstring + "MultiExponentiationBasicProofInitialMessage [_cA0=PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicA0"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], _cB=[PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], PublicCommitment [_commitment=ZpGroupElement [_value=1"
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]], _E=[CiphertextImpl [gamma=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][0]["phis"].split(",")[1]).split(";")[0] 
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]], CiphertextImpl [gamma=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["gamma"].split(";")[0])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[0]).split(";")[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[1]).split(";")[0] 
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]]]"
	
	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()
	return str(int(res,16))

def genXYZ(MNData, commlen):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	

	#Value C	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	#Value C'	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	resX = h.hexdigest()
	
	Ystring = Xstring	
	
	Ystring = Ystring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Ystring = Ystring + str(MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	Ystring = Ystring + ", _p=" + p + ", _q=" + q + "]]"

	#print Ystring
	b = Ystring.encode('utf-8')
	hy = sha256()
	hy.update(b)
	resY = hy.hexdigest()

	Zstring = Ystring + "1"

	#print Zstring
	b = Zstring.encode('utf-8')
	hz = sha256()
	hz.update(b)
	resZ = hz.hexdigest()
	
	return [resX, resY, resZ]

def genXY(MNData, commlen, cA1, cA2):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	

	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" + cA1
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + cA2
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"
	Xstring = Xstring + "HadamardProductProofInitialMessage [_cB=[PublicCommitment [_commitment=ZpGroupElement [_value="+ cA1
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]]"

	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()

	Ystring = Xstring + "1"

	#print Ystring
	b = Ystring.encode('utf-8')
	hy = sha256()
	hy.update(b)
	resY = hy.hexdigest()
	
	return [res, resY]

def genX(MNData, commlen, cA):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])

	Xstring = "PublicCommitment [_commitment=ZpGroupElement [_value=" + str(int(cA[0],16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + str(int(cA[3],16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + str(int(cA[1],16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]]PublicCommitment [_commitment=ZpGroupElement [_value="  + str(int(cA[2],16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]]ZeroProofInitialMessage [_cA0=PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicA0"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]], _cBM=PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicBM"]["element"]["value"])
	Xstring = Xstring + ", _p="+p+", _q="+q+"]], _cD=["
	for i in range(4):	
		Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][i]["element"]["value"])
		Xstring = Xstring + ", _p="+p+", _q="+q+"]], "
	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][4]["element"]["value"]) +", _p="+p+", _q="+q+"]]]]"


	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()

	return res

def SVgenX(MNData, b):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])

	Xstring = ""	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	Xstring = Xstring + "Exponent [_q=" + q +", _value="
	Xstring = Xstring + b +"]SingleValueProductProofInitialMessage [cd=PublicCommitment [_commitment=ZpGroupElement [_value="
 
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicD"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], commitmentPublicLowDelta=PublicCommitment [_commitment=ZpGroupElement [_value="

	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicLowDelta"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]], commitmentPublicHighDelta=PublicCommitment [_commitment=ZpGroupElement [_value="
	
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicHighDelta"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"

	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	resX = h.hexdigest()
		
	return resX

def SVA(debug,MNData,b):
	fw=open("outSVA.txt","w")

	fw_shf=open("resSHF.txt","r")
	
	b = fw_shf.readlines()
	fw_shf.close()

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = MNData["content"]["proofs"]["commitmentParams"]["h"]["q"]
	printf(debug,fw,int(q))

	X = SVgenX(MNData, str(int(b[5],16)))
	#print(int(b[5],16))
	#printf(debug,fw,str(b))
	printf(debug,fw,str(X))

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicD"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicLowDelta"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniSVA"]["commitmentPublicHighDelta"]["element"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentTildeR"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentTildeS"]["value"])

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentsTildeA"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansSVA"]["exponentsTildeB"][i]["value"])

	fw.close()
	

def Shuffle(debug,MNData):	
	fw=open("outSHF.txt","w")

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = int(MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,str(q))

	XYZ = genXYZ(MNData,commlen)

	printf(debug,fw,str(XYZ[0]))
	printf(debug,fw,str(XYZ[1]))
	printf(debug,fw,str(XYZ[2]))

	b = 1 
	X = int(XYZ[0],16)
	#print ("comm_length =" + str(commlen))
	#for i in range(commlen):
	#	temp = (int(XYZ[1],16) * (i + 1) + X - int(XYZ[2],16)) % int(q)
	#	X = X * X % int(q)
	#	b = b * temp % int(q)
	#	#print(i)
	#	b = b % int(q)
	#print b
	printf(debug,fw,str(b))
	printf(debug,fw,MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["gama"])
		printf(debug,fw,MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][0])
		printf(debug,fw,MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][1])

	fw.close()
	return b

def HPA(debug,MNData):	
	fw=open("outHP.txt","w")
	fw_shf=open("resSHF.txt","r")
	
	cA = fw_shf.readlines()

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = int(MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,str(q))

	XY = genXY(MNData,commlen,str(int(cA[1],16)),str(int(cA[0],16)))

	printf(debug,fw,str(XY[0]))
	printf(debug,fw,str(XY[1]))
	printf(debug,fw,str(cA[0]).replace("\n",""))
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniHPA"]["commitmentPublicB"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["iniHPA"]["commitmentPublicB"][1]["element"]["value"])
	
	#printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
	fw.close()
	fw_shf.close()
	
	return str(XY[1])

def ZeroA(debug,MNData,y):
	fw=open("outZA.txt","w")
	fw_hp=open("resHP.txt","r")
	
	cA = fw_hp.readlines()

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)

	q = int(MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,str(q))

	X = genX(MNData,commlen,cA)
	printf(debug,fw,str(X))
	printf(debug,fw,y)

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicA0"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicBM"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][0]["element"]["value"])

	printf(debug,fw,cA[0].replace("\n",""))
	printf(debug,fw,cA[2].replace("\n",""))
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][1]["element"]["value"])

	printf(debug,fw,cA[3].replace("\n",""))	
	printf(debug,fw,cA[1].replace("\n",""))
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][2]["element"]["value"])

	cD3 = MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][3]["element"]["value"]
	printf(debug,fw,cD3)

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][4]["element"]["value"])


	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentR"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentS"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentT"]["value"])

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentsA"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentsB"][i]["value"])

	fw.close()


def MEA(debug,MNData,m,cert,voteorder=0):
 
	fw=open("outMEA" +str(voteorder) +".txt","w")

	#fct = open("CTData" + str(eID) + "-" +str(i)+".txt","r")
	
	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	
	printf(debug,fw,commlen)
	printf(debug,fw,MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	
	x = MEgenX(MNData, commlen)
	printf(debug,fw,x) #need x

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicA0"]["element"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentR"]["value"]) #r

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])) #Cg_i		
		printf(debug,fw,(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][voteorder])) #Cy_i
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentsA"][i]["value"]) # a_i

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentsB"]["value"]) #b
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["exponentS"]["value"]) #s

	for i in range(2*m):
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][i]["element"]["value"])	#Bi
	
	for i in range(2*m): #E0 #E1
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][i]["gamma"].split(";")[0])
		printf(debug,fw,(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][i]["phis"].split(",")[voteorder]).split(";")[0]) 		
	
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["ansMEBasic"]["randomnessTau"]["randomnessValue"]["value"]) #tau
	
	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["group"]["g"])
	printf(debug,fw,cert[voteorder])
	fw.close()


def Verify():
	fr_shf=open("resSHF.txt","r")
	fr_ME0=open("resMEA0.txt","r")
	fr_ME1=open("resMEA1.txt","r")
	
	ME0 = fr_ME0.readlines()
	ME1 = fr_ME1.readlines()
	shf = fr_shf.readlines()
	fr_shf.close()
	fr_ME0.close()
	fr_ME1.close()

	fr_SV=open("resSV.txt","r")
	fr_ZA=open("resZA.txt","r")
	
	SV = fr_SV.readlines()
	ZA = fr_ZA.readlines()

	fr_SV.close()
	fr_ZA.close()

	if (ME0[0] != ME0[1]): return 1
	if (ME0[2] != ME0[3]): return 1
	if (ME0[4] != ME0[6]): return 1
	if (ME0[5] != ME0[7]): return 1

	if (ME1[0] != ME1[1]): return 1
	if (ME1[2] != ME1[3]): return 1
	if (ME1[4] != ME1[6]): return 1
	if (ME1[5] != ME1[7]): return 1

	if (SV[0] != SV[2]): return 1
	if (SV[1] != SV[3]): return 1

	if (ZA[0] != ZA[3]): return 1
	if (ZA[1] != ZA[4]): return 1
	if (ZA[2] != ZA[5]): return 1
	
	if (str(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["commitmentPublicB"][1]["element"]["value"]).replace(" ","") != "1"): return 1
	if (str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][3]["element"]["value"]).replace(" ","") != "1"): return 1

	if (int(MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["gamma"].split(";")[0]) != int(shf[2],16)): return 1
	if (int((MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[0]).split(";")[0]) != int(shf[3],16)): return 1
	if (int((MNData["content"]["proofs"]["secondAnswer"]["iniMEBasic"]["ciphertextsE"][1]["phis"].split(",")[1]).split(";")[0]) != int(shf[4],16)): return 1

	if (str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][3]["element"]["value"]).replace(" ","") != "1"): return 1

	return 0
if __name__ == '__main__':
	if (len(sys.argv) < 3):
		print len(sys.argv)
		print ("Usage: $python Decrypt_proof.py [Dection_Proof_Filename] [debug:optional]")
		exit(0)
	else:
		start = time.time()
		filename = str(sys.argv[1])
		debug = str(sys.argv[3])
		frDE = open(filename,"r")#"Decrypt/decryptionProof-SB1812.json", "r")
		frcert = ReadPK(str(sys.argv[2]))#ReadPK("Cert/VerifiableMixingElectoralBoard.cer")
		
		JData = frDE.read()
		MNData = json.loads(JData)
		b = Shuffle(int(debug),MNData)
		subprocess.call(["./SHFp", "outSHF.txt", debug])
		elapsed_time = time.time() - start
		print(" Shuffle argument is verified.. " + str(elapsed_time) + "s")	
		timestamp = time.time()
		
		y = HPA(int(debug),MNData)	
		subprocess.call(["./HPp", "outHP.txt", debug])
		elapsed_time = time.time() - timestamp
		print(" Hadamard Product argument is verified.. "+   str(elapsed_time) + "s")	
		timestamp = time.time()
		
		
		ZeroA(int(debug),MNData,y)	
		subprocess.call(["./ZAp", "outZA.txt", debug])
		elapsed_time = time.time() - timestamp	
		print(" Zero argument is verified.. "+   str(elapsed_time) + "s")	
		timestamp = time.time()
		
		SVA(int(debug),MNData,b)
		subprocess.call(["./SVAp", "outSVA.txt", debug])		
		elapsed_time = time.time() - timestamp
		print(" Single value argument is verified.. "+   str(elapsed_time) + "s")	
		timestamp = time.time()
				
		MEA(int(debug),MNData,1,frcert,0)
		subprocess.call(["./MEp", "outMEA0.txt", "resMEA0.txt", debug])
		elapsed_time = time.time() - timestamp
		print(" Multi Exponent argument (0) is verified.. "+   str(elapsed_time) + "s")	
		timestamp = time.time()

		MEA(int(debug),MNData,1,frcert,1)
		subprocess.call(["./MEp", "outMEA1.txt", "resMEA1.txt", debug])
		elapsed_time = time.time() - timestamp
		print(" Multi Exponent argument (1) is verified.. "+   str(elapsed_time) + "s")
		elapsed_time = time.time() - start
		print (elapsed_time)

		if Verify() : print ("Verification of " +  str(sys.argv[1]) + " failed.")
		else : print ("Verification of " +  str(sys.argv[1]) + " succeeded.")
