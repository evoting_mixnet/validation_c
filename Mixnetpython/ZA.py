import json
import subprocess
from hashlib import sha256

def printf(logging,fw,text):
	fw.write(str(text)+"\n")
	if logging > 0: print(text)
	
def genXYZ(MNData, commlen):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	

	#Value C	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	#Value C'	
	for i in range(int(commlen)):
		Xstring = Xstring + "CiphertextImpl [gamma=ZpGroupElement [_value="	
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["gama"])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], phis=[ZpGroupElement [_value=" 
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][0])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "], ZpGroupElement [_value="
		Xstring = Xstring + str(MNData["content"]["mixableOutput"]["mixable"]["cipherText"][i]["phis"][1])
		Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]]"
	
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	#print Xstring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	resX = h.hexdigest()
	
	Ystring = Xstring	
	
	Ystring = Ystring + "PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Ystring = Ystring + str(MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	Ystring = Ystring + ", _p=" + p + ", _q=" + q + "]]"

	#print Ystring
	b = Ystring.encode('utf-8')
	hy = sha256()
	hy.update(b)
	resY = hy.hexdigest()

	Zstring = Ystring + "1"

	#print Zstring
	b = Zstring.encode('utf-8')
	hz = sha256()
	hz.update(b)
	resZ = hz.hexdigest()
	
	return [resX, resY, resZ]

def genXY(MNData, commlen, cA1, cA2):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])
	Xstring = ""	

	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value=" + str(int(cA1,16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + str(int(cA2,16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"
	Xstring = Xstring + "HadamardProductProofInitialMessage [_cB=[PublicCommitment [_commitment=ZpGroupElement [_value="+ str(int(cA1,16))
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + str(MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]"

	#print Ystring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()

	Ystring = Xstring + "1"

	print Ystring
	b = Ystring.encode('utf-8')
	hy = sha256()
	hy.update(b)
	resY = hy.hexdigest()
	
	return [res, resY]

def genX(MNData, commlen, cA):
	p = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["p"])	
	q = str(MNData["content"]["proofs"]["initialMessage"][0]["element"]["q"])

	Xstring = "PublicCommitment [_commitment=ZpGroupElement [_value=" +cA[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + cA[3]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q + "]]PublicCommitment [_commitment=ZpGroupElement [_value=" + cA[0]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]]PublicCommitment [_commitment=ZpGroupElement [_value="  + cA[1]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]]ZeroProofInitialMessage [_cA0=PublicCommitment [_commitment=ZpGroupElement [_value=" 
	Xstring = Xstring + MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicA0"]["element"]["value"]
	Xstring = Xstring + ", _p=" + p + ", _q=" + q +"]], _cBM=PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicBM"]["element"]["value"]
	Xstring = Xstring + ", _p="+p+", _q="+q+"]], _cD=["
	for i in range(2m):	
		Xstring = "PublicCommitment [_commitment=ZpGroupElement [_value="
		Xstring = Xstring + MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][i]["element"]["value"]
		Xstring = Xstring + ", _p="+p+", _q="+q+"]], "
	Xstring = Xstring + "PublicCommitment [_commitment=ZpGroupElement [_value="
	Xstring = Xstring + MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][2m]["element"]["value"] +", _p="+p+", _q="+q+"]]]]"



	#print Ystring
	b = Xstring.encode('utf-8')
	h = sha256()
	h.update(b)
	res = h.hexdigest()

	return res


def Shuffle(debug,MNData,voteindex):	
	fw=open("outShuffle.txt","w")

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = int(MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,str(q))

	XYZ = genXYZ(MNData,commlen)

	printf(debug,fw,str(XYZ[0]))
	printf(debug,fw,str(XYZ[1]))
	printf(debug,fw,str(XYZ[2]))

	b = 1 
	X = int(XYZ[0],16)

	for i in range(commlen):
		temp = (int(XYZ[1],16) * (i + 1) + X - int(XYZ[2],16))
		X = X * X
		b = b * temp
	b = b % int(q)

	printf(debug,fw,str(b))
	printf(debug,fw,MNData["content"]["proofs"]["initialMessage"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["firstAnswer"][0]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["gama"])
		printf(debug,fw,MNData["content"]["mixableInput"]["mixable"]["cipherText"][i]["phis"][voteindex])

	fw.close()
	 
def HPA(debug,MNData,voteindex):	
	fw=open("outHP.txt","w")
	fw_shf=open("outSHA.txt","r")
	
	cA = shf.readlines()

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)
	q = int(MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,str(q))

	XY = genXY(MNData,commlen,cA[1],cA[0])

	printf(debug,fw,str(XY[0]))
	printf(debug,fw,str(XY[1]))
	printf(debug,fw,str(cA[0]))
	printf(debug,fw,str(cA[1]))
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["commitmentPublicB"]["element"]["value"])
	#printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["value"])

	for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
	fw.close()
	fw_shf.close()

def ZeroA(debug,MNData,m):
	fw=open("outPA.txt","w")
	fw_hpa=open("outHPA.txt","r")


	fw_shf=open("outSHA.txt","r")
	
	cA = shf.readlines()

	commlen = MNData["content"]["proofs"]["commitmentParams"]["commitmentLength"]
	printf(debug,fw,commlen)

	X = genX(MNData,commlen,cA)
	printf(debug,fw,str(X))
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicA0"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicBM"]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][0]["element"]["value"])

	printf(debug,fw,cA[0])
	printf(debug,fw,cA[2])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][1]["element"]["value"])

	printf(debug,fw,cA[3])	
	printf(debug,fw,cA[1])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][2]["element"]["value"])

	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][3]["element"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["initial"]["commitmentPublicD"][4]["element"]["value"])


	printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["h"]["q"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentR"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentS"]["value"])
	printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentT"]["value"])

        for i in range(int(commlen)):
		printf(debug,fw,MNData["content"]["proofs"]["commitmentParams"]["g"][i]["value"]) #g
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentsA"][i]["value"])
		printf(debug,fw,MNData["content"]["proofs"]["secondAnswer"]["msgPA"]["ansHPA"]["answer"]["exponentsB"][i]["value"])

	for i in range(int(commlen)):
	fw.close()
	#subprocess.call(["./SVAp", "outSVA.txt", str(debug)])	


if __name__ == '__main__':
	if (len(sys.argv) < 3):
		print len(sys.argv)
		print ("Usage: $python Decrypt_proof.py [Dection_Proof_Filename] [debug:optional]")
		exit(0)
	else:
		filename = str(sys.argv[1])
		frDE = open(filename,"r")#"Decrypt/decryptionProof-SB1812.json", "r")
		frcert = ReadPK(str(sys.argv[2]))#ReadPK("Cert/VerifiableMixingElectoralBoard.cer")

		JData = frDE.read()
		MNData = json.loads(JData)
		Shuffle(0,MNData,0)
		subprocess.call(["./Shuffle", "outShuffle.txt", str(1)])	
		HPA(debug,MNData,voteindex):
		ZeroA(debug,MNData,m):
		

