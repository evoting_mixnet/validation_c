
## Requirement
You need openssl-dev to compile C files. 

1. Go to MixnetC (i.e., cd MixnetC)
2. Compile the C files (i.e., make)
3. You can see the execution file was created under Mixnetpython folder.

## Execution
1. go to Mixnetpython folder.
2. execute "$python mixing_proof.py [Mixing_Proof_Foldername] [Certificate] 1".
   For example, $python Mixing_proof.py mixing_proofs_SB2101_SB2101_20210522_124242 /Certificates/VerifiableMixingElectoralBoard.cer 1